import matplotlib.pyplot as plt

from .smath import pred_types

MOUTH_LINES = [slice(48,53),slice(53,60),slice(60,64),slice(64,68)]
MOUTH_LINES_ONLY_MOUTH = [slice(0,6),slice(6,12),slice(12,16),slice(16,20)]
FACE_LINES = [slice(0, 17), slice(17, 22), slice(22, 27), slice(27, 31), slice(31, 36), slice(36, 42), slice(42, 48), slice(48, 60), slice(60, 68)]


def smart_plot(faces, titles=None,lines = None, color = None,xlim=None,ylim=None):
    if type(faces) is not list:
        faces = [faces]
    if titles and type(titles) is not list:
        titles = [titles]

    face_count = len(faces)
    fig = plt.figure(figsize=(10, 10), dpi=100, )
    width = 3 if face_count >= 3 else face_count
    height = int(face_count / 3) + 1

    if len(faces[0][0]) == 3:
        for idx, face in enumerate(faces):
            ax = fig.add_subplot(height, width, idx + 1, projection='3d')
            surf = ax.scatter(face[:, 0],
                              face[:, 1],
                              face[:, 2],
                              c='cyan',
                              alpha=1.0,
                              edgecolor='b')

            if lines:
                for line in lines:
                    ax.plot3D(face[line, 0],
                              face[line, 1],
                              face[line, 2], color='blue')

            for i, d in enumerate(face):
                plt.annotate(str(i), (face[i, 0], face[i, 1]),clip_on=True)

            ax.view_init(elev=90., azim=90.)
            ax.set_xlim(ax.get_xlim()[::-1]) if not xlim else ax.set_xlim(xlim)
            ax.set_ylim(ax.get_ylim()[::-1]) if not ylim else ax.set_ylim(ylim)



            title = idx if type(titles) is not list else titles[idx]
            ax.title.set_text(title)

    if len(faces[0][0]) == 2:
        for idx, face in enumerate(faces):
            ax = fig.add_subplot(height, width, idx + 1)
            ax.scatter(face[:, 0],
                              face[:, 1],
                              c='cyan',
                              alpha=1.0,
                              edgecolor='b')
            if lines:
                for line in lines:
                    ax.plot(face[line, 0],
                            face[line, 1],
                            color='blue')
            for i, d in enumerate(face):
                plt.annotate(str(i), (face[i, 0], face[i, 1]),clip_on=True)

            title = idx if type(titles) is not list else titles[idx]

            ax.set_xlim(ax.get_xlim()) if not xlim else ax.set_xlim(xlim)
            ax.set_ylim(ax.get_ylim()) if not ylim else ax.set_ylim(ylim)

            ax.title.set_text(title)

    plt.show()


def plot_faces(faces, images, lines=[],titles=[]):
    face_count = len(faces)
    fig = plt.figure(figsize=(30, 30), dpi=200, )
    width = 3 if face_count >= 3 else face_count
    height = int(face_count / 3) + 1
    for idx, face in enumerate(faces):
        ax = fig.add_subplot(height, width, idx + 1)
        ax.imshow(images[idx])

        for line in lines:
            ax.plot(face[line, 0],
                    face[line, 1],
                    color='blue')
        ax.axis('off')
        title = idx if not titles else titles[idx]
        ax.title.set_text(title)


    plt.show()

