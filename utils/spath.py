import os
from glob import glob
from pathlib import Path
from .interact import interact as io

def abspath(p):
    return Path(os.path.abspath(p))


def relpath(p1, p2):
    return Path(os.path.relpath(str(p1), p2))


def path_extension(p):
    return os.path.splitext(p)[1]


def clean_path(p,string=False):
    # without the extension
    return str(os.path.splitext(p)[0]) if string else os.path.splitext(p)[0]

def dirname(p):
    return Path(os.path.dirname(p))


def output_path_maintain_structure(main_path, file_path, output_path):
    return abspath(output_path / relpath(dirname(file_path), main_path))

def get_output_path(in_path, filepath, output_path):
    absdir = abspath(output_path)
    reldir = str(relpath(clean_path(filepath), in_path))  # get the path for the file
    return absdir / reldir


