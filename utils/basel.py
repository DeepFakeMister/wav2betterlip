import h5py
import numpy as np
import torch
import matplotlib.pyplot as plt
from scipy.optimize import lsq_linear
from scipy.optimize import minimize
from tqdm.auto import tqdm
from utils.smath import get_umeyama_transformed
from pathlib import Path
import utils.interact.interact as io


class Basel():
    def __init__(self, checkpoints="checkpoints", device="cpu"):
        model_path = Path(checkpoints) / "basel2017.h5"
        landmarks_path = Path(checkpoints) / "Landmarks68_model2017-1_face12_nomouth.anl"

        self.device = device
        bfm = h5py.File(model_path, 'r')
        self.landmarks = np.loadtxt(landmarks_path, dtype=np.int32)

        # the mean location of the vertices
        shape_mean = np.asarray(bfm['shape/model/mean'], dtype=np.float32)
        self.shape_means_3d = torch.from_numpy(np.reshape(shape_mean, (-1, 3)))[self.landmarks, :].to(device)
        self.shape_means_2d = self.shape_means_3d[:, :2]

        # neutral principal components of neutral shape
        shape_basis = np.asarray(bfm['shape/model/pcaBasis'], dtype=np.float32)

        self.shape_basis_3d = torch.from_numpy(np.reshape(shape_basis, (-1, 3, 199)))[self.landmarks, :, :].to(device)
        self.shape_basis_2d = self.shape_basis_3d[:, :2, :]

        # how the shape can variate
        self.shape_variance = torch.from_numpy(np.asarray(bfm['shape/model/pcaVariance'], dtype=np.float32)).to(device)

        # the mean of the expressions
        expression_means = np.asarray(bfm['expression/model/mean'], dtype=np.float32)
        self.expression_means_3d = torch.from_numpy(np.reshape(expression_means, (-1, 3)))[self.landmarks, :].to(device)
        self.expression_means_2d = self.expression_means_3d[:, :2]

        # the neutral principal components of the expressions
        expression_basis = np.asarray(bfm['expression/model/pcaBasis'], dtype=np.float32)
        self.expression_basis_3d = torch.from_numpy(np.reshape(expression_basis, (-1, 3, 100)))[self.landmarks, :,
                                   :].to(device)  # this correct?
        self.expression_basis_2d = self.expression_basis_3d[:, :2]

        # how the expression can variate
        self.expression_variance = torch.from_numpy(
            np.asarray(bfm['expression/model/pcaVariance'], dtype=np.float32)).to(device)

        # facial mesh triangles
        self.triangles = np.swapaxes(np.asarray(bfm['shape/representer/cells'], dtype=np.int32), 0, 1)

        # dlib triangles

        self._canonical = None

    def get_expression(self, expression_matrix, dimensions,add_means=True):
        if dimensions == 2:
            if len(expression_matrix.shape) == 2:
                # batch
                expression = self.expression_basis_2d @ (expression_matrix * self.expression_variance ** 0.5).T
                expression = expression.permute(2, 0, 1)
            else:
                expression = (
                        self.expression_basis_2d @ (expression_matrix * self.expression_variance ** 0.5))
            if add_means:
                expression = expression + self.expression_means_2d
        elif dimensions == 3:
            expression = (
                    self.expression_basis_3d @ (expression_matrix * self.expression_variance ** 0.5))
            if add_means:
                expression = expression + self.expression_means_3d
        else:
            raise Exception("dimensions must be 2d or 3d")
        return expression

    def get_shape(self, shape_matrix, dimensions,add_means=True):
        if dimensions == 2:
            if len(shape_matrix.shape) == 2:
                shape = self.shape_basis_2d @ (shape_matrix * self.shape_variance ** 0.5).T
                shape = shape.permute(2, 0, 1)
            else:
                shape = self.shape_basis_2d @ (shape_matrix * self.shape_variance ** 0.5)
            if add_means:
                shape+=self.shape_means_2d
        elif dimensions == 3:
            shape = self.shape_means_3d + (
                    self.shape_basis_3d @ (shape_matrix * self.shape_variance ** 0.5))
        else:
            raise Exception("dimensions must be 2d or 3d")

        return shape

    def normalize(self, data):
        if (type(data) == np.ndarray): data = torch.from_numpy(data)
        data = data - data.min(-2, keepdim=True)[0]
        return data / data.max(-2, keepdim=True)[0]

    def fit_2d(self, dlib,idxs, alpha=10 ** -1, steps = 3000):
        dlib = self.normalize(dlib)
        shape_matrix = torch.randn((dlib.shape[0],199), requires_grad=True, device=self.device)
        expression_matrix = torch.randn((dlib.shape[0],100), requires_grad=True, device=self.device)

        optimizer = torch.optim.Adam([shape_matrix, expression_matrix])

        losses_landmarks = []
        losses_regularized = []
        #first find out the shape
        for idx in tqdm(range(steps)):
            optimizer.zero_grad()
            face = self.get_face(shape_matrix, expression_matrix, normalized=True, dimensions=2)

            landmarks_loss = torch.log((1 / 68) * ((face - dlib) ** 2).sum())  # we add more weight to the mouth
            regularization_loss = alpha * torch.log((shape_matrix ** 2).sum() + (expression_matrix ** 2).sum())
            loss = landmarks_loss + regularization_loss
            loss.backward()
            optimizer.step()
            losses_landmarks.append((landmarks_loss).detach())
            losses_regularized.append(regularization_loss.detach())

        # io.log_info(f"landmark_loss: {losses_landmarks[::500]}")
        # io.log_info(f"regularization_loss: {losses_regularized[::500]}")
        shapes = []
        for slic in idxs:
            mat = shape_matrix[slic]
            mean_mat = mat.mean(dim=0)
            shapes.append(mean_mat.repeat((mat.shape[0],1)))
        shape_matrix = torch.cat(shapes)
        print(shape_matrix.shape)

        optimizer = torch.optim.Adam([expression_matrix])

        losses_landmarks = []
        losses_regularized = []

        for idx in tqdm(range(steps)):
            optimizer.zero_grad()
            face = self.get_face(shape_matrix, expression_matrix, normalized=True, dimensions=2)

            mouth_loss = torch.log(1 / 20 * (face[:,48:68, :] - dlib[:,48:68, :]).pow(2).sum())
            landmarks_loss = torch.log((1 / 68) * ((face - dlib) ** 2).sum())  # we add more weight to the mouth
            regularization_loss = alpha * torch.log((shape_matrix ** 2).sum() + (expression_matrix ** 2).sum())
            loss = mouth_loss + landmarks_loss + regularization_loss
            loss.backward()
            optimizer.step()
            losses_landmarks.append((landmarks_loss + mouth_loss).detach())
            losses_regularized.append(regularization_loss.detach())
        # io.log_info(f"landmark_loss: {losses_landmarks[::500]}")
        # io.log_info(f"regularization_loss: {losses_regularized[::500]}")

        return shape_matrix.detach(), expression_matrix.detach()


    def get_face(self, shape_matrix=None, expression_matrix=None, normalized=False, dimensions=2):
        if shape_matrix == None: shape_matrix = torch.zeros(199).to(self.device)
        if expression_matrix == None: expression_matrix = torch.zeros(100).to(self.device)

        shape = self.get_shape(shape_matrix, dimensions)
        expression = self.get_expression(expression_matrix, dimensions)

        return self.normalize(shape + expression) if normalized else shape + expression

    def plot(self, shape_matrix, expression_matrix, title=""):
        with torch.no_grad():
            mesh = self.normalize(self.get_face(shape_matrix, expression_matrix)).cpu()

            plt.figure()
            plt.scatter(x=mesh[:, 0], y=mesh[:, 1])
            plt.title(title)
            for idx, d in enumerate(mesh):
                plt.annotate(str(idx), (mesh[idx, 0], mesh[idx, 1]))
            plt.show()

    def plot_dlib(self, dlib, title=""):
        dlib = self.normalize(dlib).cpu()
        with torch.no_grad():
            plt.figure()
            plt.scatter(x=dlib[:, 0], y=dlib[:, 1])
            plt.title(title)
            for idx, d in enumerate(dlib):
                plt.annotate(str(idx), (dlib[idx, 0], dlib[idx, 1]))
            plt.show()

    def allign_face(self, points, dimensions=None):
        if (isinstance(points, torch.Tensor)): points = points.numpy()
        if dimensions == None:
            dimensions = points.shape[1]
        face = self.get_face(dimensions=dimensions)

        transformed = get_umeyama_transformed(points, canonical=face)
        return self.normalize(transformed)

if __name__ == "__main__":
    device = "cpu"
    basel = Basel(device=device)
    basel.plot_dlib(basel.get_face())
    for x in range(5):
        print("running optimization")
        shape_matrix = torch.randn(199).to(device)
        expression_matrix = torch.randn(100).to(device)
    #
        fitted_shape, fitted_expression = basel.fit_2d(basel.get_face(shape_matrix, expression_matrix))
        basel.plot(shape_matrix, expression_matrix, "target")
        basel.plot(fitted_shape, fitted_expression, "fitted")
