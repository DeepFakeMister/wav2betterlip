from utils.spath import *
import torch
from tqdm import tqdm
import h5py
import numpy as np
import pandas as pd


def bundle(file_path, key, grp, extension):
    d = torch.load(str(file_path) + "." + extension)
    grp[key] = d.numpy()

    # expression = torch.load(p + ".expression")
    # shape = torch.load(p + ".shape")
    # landmarks = torch.load(p + ".landmarks")
    # landmarks_aligned = torch.load(p + ".landmarks_aligned")
    # pass


extensions = ["deepspeech", "expression", "shape", "landmarks", "landmarks_aligned"]


def structure_dir(filelist,args):
    dirs = {}
    for file in filelist:
        p = clean_path(file)
        full_dir = relpath(dirname(p),args.in_dir)
        full_dir = str(full_dir)
        if not all([os.path.exists(clean_path(p) + f".{extension}") for extension in extensions]): continue
        if full_dir not in dirs:
            dirs[full_dir] = []
        dirs[full_dir].append(Path(file).stem)
    return dirs


def load_txt(pth):
    files = set()
    with open(pth) as f:
        while(l:=f.readline()):
            files.add(l.split()[0])
    return files

def _main(args):
    h5test = h5py.File(args.out+"test.h5", 'w')
    h5val = h5py.File(args.out+"val.h5", 'w')
    h5train = h5py.File(args.out+"train.h5", 'w')

    h5s = [h5test,h5val,h5train]

    # dset = h5.create_dataset

    test = load_txt(args.test)
    val = load_txt(args.val)
    train = load_txt(args.train)


    filelist = glob(os.path.join(args.in_dir, '**/*.jpeg'), recursive=True)

    filelist = structure_dir(filelist,args)

    for h5 in h5s:
        for extension in extensions:
            grp = h5.create_group(extension)

    for vidpath, vid_files in tqdm(filelist.items()):

        #ensures the relevant paths are there
        for h in h5s:
            if vidpath in h:
                h5=h
                for extension in extensions:
                    h5[extension].create_group(vidpath)
                break

        #add the relevant files
        for file in vid_files:
            for extension in extensions:
                grp = h5[extension][vidpath]
                bundle(f"{vidpath}/{file}", file, grp, extension)

        h5= None


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Bundle the dataset')
    parser.add_argument("-in_dir", type=str, default="workspace/")
    parser.add_argument("-out", type=str, default="workspace/")
    parser.add_argument("-size", type=int, default=10 ** 10)
    parser.add_argument("-test", type=str, default="data/test.txt")
    parser.add_argument("-train", type=str, default="data/train.txt")
    parser.add_argument("-val", type=str, default="data/val.txt")

    args = parser.parse_args()
    _main(args)
