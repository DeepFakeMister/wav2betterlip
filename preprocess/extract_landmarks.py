import torch.multiprocessing as mp

import decord
import face_alignment
from decord import VideoReader
from decord import cpu
from tqdm import tqdm

from utils.spath import *
from PIL import Image

from utils.basel import *

decord.bridge.set_bridge('torch')
from threading import Thread
import torch
def get_correct_path(in_path, filepath, output_path):
    absdir = abspath(output_path)
    reldir = str(relpath(clean_path(filepath), in_path))  # get the path for the file
    return absdir / reldir



class NoFaceException(Exception):
    pass

def extract_landmarks(frame, id):
    # can add batch
    preds = FAN.get_landmarks(frame)
    if not preds:
        io.log_err("no face detected:" + str(id))
        raise NoFaceException
    return preds[0]

def process_frame(frame, idx):
    landmarks = extract_landmarks(frame, idx)
    return landmarks


def process_video(a):
    video_path, landmarks_out, image_out, args = a
    landmarks_out.mkdir(parents=True, exist_ok=True)
    image_out.mkdir(parents=True, exist_ok=True)
    vr = VideoReader(str(video_path), ctx=cpu(0))
    for i in range(len(vr)):
        frame = vr.next()
        try:
            j = process_frame(frame, i)
        except NoFaceException:
            continue

        write_json(j,landmarks_out,i)
        Thread(target=lambda:write_image(frame,image_out,i)).start()

def write_json(processed_d, output_path, idx):
    p = output_path / f"{str(idx).zfill(5)}.landmarks"
    torch.save(torch.Tensor(processed_d),p)

def write_image(frame,output_path,idx):
    im = Image.fromarray(frame.numpy())
    im.save(output_path / f"{str(idx).zfill(5)}.jpeg",normalize=False)

def init_models(q):
    global FAN
    FAN = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D, flip_input=False, device="cuda:"+str(q.get()))
    io.log_info("models loaded")


def _main(args):
    filelist = glob(os.path.join(args.in_dir, '**/*.mp4'), recursive=True)

    jobs = []
    for file_path in tqdm(filelist):
        jobs.append((Path(file_path), get_correct_path(args.in_dir, file_path, args.landmarks_out),get_correct_path(args.in_dir, file_path, args.image_out),args))
    q = mp.Queue()
    [q.put(device) for device in args.devices]
    with mp.Pool(processes=len(args.devices),initializer=init_models,initargs=[q]) as pool:
        _iter = pool.imap_unordered(process_video, jobs)
        for i in tqdm(range(len(jobs))):
            next(_iter)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Extract dataset from videos')
    parser.add_argument("-in_dir", type=str, default="workspace/lotr")
    parser.add_argument("-landmarks_out", type=str, default="workspace")
    parser.add_argument("-image_out", type=str, default="workspace")
    parser.add_argument("-devices", type=int,nargs="*", default=[0])

    args = parser.parse_args()

    _main(args)
