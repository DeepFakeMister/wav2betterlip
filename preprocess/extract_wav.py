import decord

from utils.spath import *

decord.bridge.set_bridge('torch')
import subprocess
import torch.multiprocessing as mp
from tqdm import tqdm


def extract(a):
    input_file, output_path = a
    output_path.mkdir(parents=True, exist_ok=True)
    command = f'ffmpeg -loglevel panic -y -i {input_file} -strict -2 {output_path / "audio.wav"}'
    subprocess.call(command, shell=True)


def _main(args):
    filelist = glob(os.path.join(args.in_dir, '**/*.mp4'), recursive=True)

    with mp.Pool(processes=args.workers) as pool:
        jobs = []
        for file_path in tqdm(filelist):
            ouput_path = get_output_path(args.in_dir, file_path, args.out_dir)
            jobs.append((file_path, ouput_path))
        _iter = pool.imap_unordered(extract, jobs)
        for i in tqdm(range(len(filelist))):
            next(_iter)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Extract dataset from videos')
    parser.add_argument("-in_dir", type=str, default="data/unprocessed")
    parser.add_argument("-out_dir", type=str, default="workspace/")
    parser.add_argument("-workers", type=int, default=2)

    args = parser.parse_args()

    _main(args)
