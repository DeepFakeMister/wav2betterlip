import decord
from tqdm import tqdm

from utils.basel import Basel
from utils.spath import *

decord.bridge.set_bridge('torch')
import torch
import torch.multiprocessing as mp

basel = None
basel_cpu=None

def extract(batch_paths,out_paths,idxs,args):

    global basel
    global basel_cpu
    if basel == None:
        basel = Basel(checkpoints=args.checkpoints,device=f'cuda:{args.gpu}')
        basel_cpu = Basel(checkpoints=args.checkpoints)
    for out_path in out_paths:
        out_path.mkdir(parents=True, exist_ok=True)

    landmarks =[]
    for idx,path in enumerate(batch_paths):
         landmarks.append(basel_cpu.allign_face(torch.load(path)))
    landmarks = torch.stack(landmarks)

    for slic, out_path in zip(idxs,out_paths):
        torch.save(landmarks[slic,:,:2].clone(), str(out_path / "full.landmarks_aligned"))

    landmarks = landmarks.to(f'cuda:{args.gpu}')

    shape_matrix, expression_matrix = basel.fit_2d(dlib=landmarks[:,:, :2],idxs=idxs, alpha=10 ** -2,steps=3000)

    for idx ,slic in zip(range(len(idxs)),idxs):
        out_path = out_paths[idx]
        torch.save(shape_matrix[idx].cpu().clone(), str(out_path / f"full.shape"))
        torch.save(expression_matrix[slic].cpu().clone(), str(out_path / f"full.expression"))


def _main(args):
    filelist = glob(os.path.join(args.in_dir, '**/*.landmarks'), recursive=True)
    filelist.sort()
    dirs = {}
    for idx, file_path in tqdm(enumerate(filelist)):
           dirs.setdefault(dirname(file_path),[]).append(file_path)

    batch_idx = 0
    aligned_paths = []
    batch_paths = []
    idxs = []
    prev_idx = 0
    for idx,paths in enumerate(tqdm(dirs.values())):

        aligned_path = get_output_path(args.in_dir, dirname(paths[0]), args.out_aligned)
        aligned_paths.append(aligned_path)
        batch_paths+=paths
        idxs.append(slice(prev_idx,prev_idx+len(paths)))
        prev_idx+=len(paths)
        batch_idx+=1
        if batch_idx == args.batch or idx==(len(dirs)-1):
            pass
        else: continue

        extract(batch_paths,aligned_paths,idxs,args)

        batch_idx = 0
        aligned_paths = []
        batch_paths = []
        idxs = []
        prev_idx = 0

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Extract dataset from videos')
    parser.add_argument("-in_dir", type=str, default="workspace/lotr")
    parser.add_argument("-out_aligned", type=str, default="workspace/lotr")
    parser.add_argument("-out_basel", type=str, default="workspace")
    parser.add_argument("-checkpoints", type=str, default="checkpoints/")
    parser.add_argument("-gpu", type=int, default=0)
    parser.add_argument("-batch", type=int, default=4)

    args = parser.parse_args()

    _main(args)

import gc

import datetime


