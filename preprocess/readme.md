# Preprocessing functions
steps:

1. extract_landmarks.py extracts all images and facial landmarks from a directory of video's and stores them in workspace/vidpath
	 \- devices: [0,1,2..] list the gpu divices
	 \- in_dir: "/path/to/videos"

2. align_and_extract.py first aligns the landmark to the canonical face ("/workspace/.../full.landmarks_aligned), and then extracts the shape and expression vectors ("/workspace/.../full.shape",  "/workspace/.../full.expression")
  \- gpu: determine the gpu
  \- batch : int, set the batch size

 3. extract_wav.py extracts all wavs from the videos.
 4. extract_audio_features_clean.py extracts all voca audio features from the wavs

