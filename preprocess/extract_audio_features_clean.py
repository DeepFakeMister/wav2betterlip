from scipy.io import wavfile
from tqdm import tqdm

from utils.spath import *
import multiprocessing as mp
import os


def read(read_queue, gpu_queue):
    while True:
        job = read_queue.get()
        audio_path, out_path = job
        sample_rate, audio = wavfile.read(audio_path)
        gpu_queue.put((sample_rate, audio, out_path))


def run_gpu(gpu_queue, write_queue, device, args):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    from voca.utils.audio_handler import AudioHandler
    import tensorflow as tf

    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
    os.environ["CUDA_VISIBLE_DEVICES"] = str(device) if device > -1 else  ""

    config = {}
    config['deepspeech_graph_fname'] = args.model_path
    config['audio_feature_type'] = 'deepspeech'
    config['num_audio_features'] = 29

    config['audio_window_size'] = 16
    config['audio_window_stride'] = 1
    AUDIO = AudioHandler(config)

    while True:
        job = gpu_queue.get()
        sample_rate, audio, out_path = job
        print(f"gpu:{device} ")

        tmp_audio = {'subj': {'seq': {'audio': audio, 'sample_rate': sample_rate}}}
        processed_audio = AUDIO.process(tmp_audio, args.fps)['subj']['seq']['audio']
        write_queue.put((processed_audio, out_path))


def write(write_queue, done_queue):
    import torch
    while True:
        job = write_queue.get()
        processed_audio, out_path = job

        num_frames = processed_audio.shape[0]
        for i in range(0, num_frames):
            fname = out_path / (str(i).zfill(5) + '.deepspeech')
            torch.save(torch.Tensor(processed_audio[i]), fname)
        done_queue.put(None)


def _main(args):
    filelist = glob(os.path.join(args.in_dir, '**/*.wav'), recursive=True)
    jobs = []
    for file_path in tqdm(filelist):
        out_path = args.out_dir / relpath(dirname(file_path), args.in_dir)
        jobs.append([file_path, out_path])

    read_queue = mp.Queue()
    gpu_queue = mp.Queue()
    write_queue = mp.Queue()
    done_queue = mp.Queue()

    read_pool = [mp.Process(target=read, args=(read_queue, gpu_queue)).start() for _ in range(args.write_workers)]
    gpu_pool = [mp.Process(target=run_gpu,
                           args=(gpu_queue, write_queue, device, args)).start()
                for device in args.devices]
    write_pool = [mp.Process(target=write, args=(write_queue, done_queue)).start() for _ in range(args.read_workers)]

    for job in tqdm(jobs):
        read_queue.put(job)

    for job in tqdm(range(len(jobs))):
        done_queue.get()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Extract dataset from videos')
    parser.add_argument("-in_dir", type=str, default="workspace")
    parser.add_argument("-out_dir", type=str, default="workspace")
    parser.add_argument("-model_path", type=str, default="checkpoints/voca.pb")
    parser.add_argument("-fps", type=int, default=25)
    parser.add_argument("-devices", type=int, nargs="+", default=[-1])
    parser.add_argument("-read_workers", type=int, default=2)
    parser.add_argument("-write_workers", type=int, default=2)

    args = parser.parse_args()

    _main(args)
