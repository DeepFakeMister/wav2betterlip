from scipy.io import wavfile
from tqdm import tqdm

from utils.spath import *
import os
import torch

# from pympler.tracker import SummaryTracker
# tracker = SummaryTracker()

def init_models(args):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
    os.environ["CUDA_VISIBLE_DEVICES"] = args.device

    from voca.utils.audio_handler import AudioHandler

    global AUDIO
    global GPU
    config = {}
    config['deepspeech_graph_fname'] = args.model_path
    config['audio_feature_type'] = 'deepspeech'
    config['num_audio_features'] = 29

    config['audio_window_size'] = 16
    config['audio_window_stride'] = 1
    AUDIO = AudioHandler(config)



def run_audio_handler(audio_handler, audiopath, fps):
    sample_rate, audio = wavfile.read(audiopath)
    if audio.ndim != 1:
        print('Audio has multiple channels, only first channel is considered')
        audio = audio[:, 0]

    tmp_audio = {'subj': {'seq': {'audio': audio, 'sample_rate': sample_rate}}}
    processed_audio = audio_handler.process(tmp_audio, fps)['subj']['seq']['audio']
    return processed_audio


def extract(a):
    file_path, out_path, args = a
    fname = str(out_path / (str("").zfill(5) + '.deepspeech'))
    if os.path.exists(fname):
        print("skipping because exists: " + fname)
        return

    out_path.mkdir(parents=True, exist_ok=True)
    processed_audio = run_audio_handler(AUDIO, file_path, args.fps)
    # fps does not match the video framerate
    # print('audio processed type:', type(processed_audio))
    # print('audio processed shape:', processed_audio.shape)
    num_frames = processed_audio.shape[0]
    # print('num_frames:', num_frames)

    for i in range(0, num_frames):
        fname = out_path / (str(i).zfill(5) + '.deepspeech')
        torch.save(torch.Tensor(processed_audio[i]), fname)

import random

def _main(args):
    init_models(args)
    filelist = glob(os.path.join(args.in_dir, '**/*.wav'), recursive=True)
    random.shuffle(filelist)
    jobs = []
    for file_path in tqdm(filelist):
        out_path = args.out_dir / relpath(dirname(file_path), args.in_dir)
        extract((file_path, out_path, args))

    # q = mp.Queue()
    # [q.put(device) for device in args.devices]
    # with mp.Pool(processes=len(args.devices), initializer=init_models, initargs=[q, args]) as pool:
    #
    #     _iter = pool.imap(extract, jobs)
    #     for i in tqdm(range(len(jobs))):
    #         next(_iter)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Extract dataset from videos')
    parser.add_argument("-in_dir", type=str, default="workspace")
    parser.add_argument("-out_dir", type=str, default="workspace")
    parser.add_argument("-model_path", type=str, default="checkpoints/voca.pb")
    parser.add_argument("-fps", type=int, default=25)
    parser.add_argument("-device", type=str, default="0")

    args = parser.parse_args()

    _main(args)
