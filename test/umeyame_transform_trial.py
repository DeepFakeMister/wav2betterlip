import numpy as np
import face_alignment
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from skimage import io
import collections

from core.mathlib import umeyama
from facelib import LandmarksProcessor, FaceType
import cv2


face_detector = 'sfd'
# Run the 3D face alignment on a test image, without CUDA.
fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D, device='cuda', flip_input=True,
                                  face_detector=face_detector)


for i in range(10):
    try:
        input_img = io.imread(f'D:\\GDrive\\Scriptie\\wav2betterlip\\data\\processed\\5537893465701857051\\00001\\{i}.jpg')
    except FileNotFoundError:
        input_img = io.imread('test/assets/aflw-test.jpg')

    facepoints = fa.get_landmarks(input_img)[-1]


    landmarks_68_3D = np.array([
        [-73.393523, -29.801432, 47.667532],  # 00
        [-72.775014, -10.949766, 45.909403],  # 01
        [-70.533638, 7.929818, 44.842580],  # 02
        [-66.850058, 26.074280, 43.141114],  # 03
        [-59.790187, 42.564390, 38.635298],  # 04
        [-48.368973, 56.481080, 30.750622],  # 05
        [-34.121101, 67.246992, 18.456453],  # 06
        [-17.875411, 75.056892, 3.609035],  # 07
        [0.098749, 77.061286, -0.881698],  # 08
        [17.477031, 74.758448, 5.181201],  # 09
        [32.648966, 66.929021, 19.176563],  # 10
        [46.372358, 56.311389, 30.770570],  # 11
        [57.343480, 42.419126, 37.628629],  # 12
        [64.388482, 25.455880, 40.886309],  # 13
        [68.212038, 6.990805, 42.281449],  # 14
        [70.486405, -11.666193, 44.142567],  # 15
        [71.375822, -30.365191, 47.140426],  # 16
        [-61.119406, -49.361602, 14.254422],  # 17
        [-51.287588, -58.769795, 7.268147],  # 18
        [-37.804800, -61.996155, 0.442051],  # 19
        [-24.022754, -61.033399, -6.606501],  # 20
        [-11.635713, -56.686759, -11.967398],  # 21
        [12.056636, -57.391033, -12.051204],  # 22
        [25.106256, -61.902186, -7.315098],  # 23
        [38.338588, -62.777713, -1.022953],  # 24
        [51.191007, -59.302347, 5.349435],  # 25
        [60.053851, -50.190255, 11.615746],  # 26
        [0.653940, -42.193790, -13.380835],  # 27
        [0.804809, -30.993721, -21.150853],  # 28
        [0.992204, -19.944596, -29.284036],  # 29
        [1.226783, -8.414541, -36.948060],  # 00
        [-14.772472, 2.598255, -20.132003],  # 01
        [-7.180239, 4.751589, -23.536684],  # 02
        [0.555920, 6.562900, -25.944448],  # 03
        [8.272499, 4.661005, -23.695741],  # 04
        [15.214351, 2.643046, -20.858157],  # 05
        [-46.047290, -37.471411, 7.037989],  # 06
        [-37.674688, -42.730510, 3.021217],  # 07
        [-27.883856, -42.711517, 1.353629],  # 08
        [-19.648268, -36.754742, -0.111088],  # 09
        [-28.272965, -35.134493, -0.147273],  # 10
        [-38.082418, -34.919043, 1.476612],  # 11
        [19.265868, -37.032306, -0.665746],  # 12
        [27.894191, -43.342445, 0.247660],  # 13
        [37.437529, -43.110822, 1.696435],  # 14
        [45.170805, -38.086515, 4.894163],  # 15
        [38.196454, -35.532024, 0.282961],  # 16
        [28.764989, -35.484289, -1.172675],  # 17
        [-28.916267, 28.612716, -2.240310],  # 18
        [-17.533194, 22.172187, -15.934335],  # 19
        [-6.684590, 19.029051, -22.611355],  # 20
        [0.381001, 20.721118, -23.748437],  # 21
        [8.375443, 19.035460, -22.721995],  # 22
        [18.876618, 22.394109, -15.610679],  # 23
        [28.794412, 28.079924, -3.217393],  # 24
        [19.057574, 36.298248, -14.987997],  # 25
        [8.956375, 39.634575, -22.554245],  # 26
        [0.381549, 40.395647, -23.591626],  # 27
        [-7.428895, 39.836405, -22.406106],  # 28
        [-18.160634, 36.677899, -15.121907],  # 29
        [-24.377490, 28.677771, -4.785684],  # 30
        [-6.897633, 25.475976, -20.893742],  # 31
        [0.340663, 26.014269, -22.220479],  # 32
        [8.444722, 25.326198, -21.025520],  # 33
        [24.474473, 28.323008, -5.712776],  # 34
        [8.449166, 30.596216, -20.671489],  # 35
        [0.205322, 31.408738, -21.903670],  # 36
        [-7.198266, 30.844876, -20.328022]  # 37
    ], dtype=np.float32)




    #normalize everything to image range
    pts2 = np.float32(( (0,0),(160,0),(160,160) ))
    centered_face  = facepoints - facepoints.mean(axis=0)
    landmarks_68_3D  = landmarks_68_3D #- landmarks_68_3D.mean(axis=0)



    def transform_points(points, mat, invert=False):
        if invert:
            mat = np.linalg.inv(mat)
        ones = np.zeros(points.shape[0])
        points = np.column_stack((points, ones))
        points = (mat@points.T).T
        points = points[:, :3]
        return points


    pred_type = collections.namedtuple('prediction_type', ['slice', 'color'])
    pred_types = {'face': pred_type(slice(0, 17), (0.682, 0.780, 0.909, 0.5)),
                  'eyebrow1': pred_type(slice(17, 22), (1.0, 0.498, 0.055, 0.4)),
                  'eyebrow2': pred_type(slice(22, 27), (1.0, 0.498, 0.055, 0.4)),
                  'nose': pred_type(slice(27, 31), (0.345, 0.239, 0.443, 0.4)),
                  'nostril': pred_type(slice(31, 36), (0.345, 0.239, 0.443, 0.4)),
                  'eye1': pred_type(slice(36, 42), (0.596, 0.875, 0.541, 0.3)),
                  'eye2': pred_type(slice(42, 48), (0.596, 0.875, 0.541, 0.3)),
                  'lips': pred_type(slice(48, 60), (0.596, 0.875, 0.541, 0.3)),
                  'teeth': pred_type(slice(60, 68), (0.596, 0.875, 0.541, 0.4))
                  }

    fig = plt.figure(figsize=plt.figaspect(.5))
    ax = fig.add_subplot(2, 3, 1, projection='3d')

    # plot 1
    surf = ax.scatter(centered_face[:, 0],
                      centered_face[:, 1],
                      centered_face[:, 2],
                      c='cyan',
                      alpha=1.0,
                      edgecolor='b')

    for pred_type in pred_types.values():
        ax.plot3D(centered_face[pred_type.slice, 0],
                  centered_face[pred_type.slice, 1],
                  centered_face[pred_type.slice, 2], color='blue')

    ax.view_init(elev=90., azim=90.)
    ax.set_xlim(ax.get_xlim()[::-1])
    ax.title.set_text("input_face")


    ax = fig.add_subplot(2, 3, 2, projection='3d')
    surf = ax.scatter(landmarks_68_3D[:, 0],
                      landmarks_68_3D[:, 1],
                      landmarks_68_3D[:, 2],
                      c='cyan',
                      alpha=1.0,
                      edgecolor='b')

    for pred_type in pred_types.values():
        ax.plot3D(landmarks_68_3D[pred_type.slice, 0],
                  landmarks_68_3D[pred_type.slice, 1],
                  landmarks_68_3D[pred_type.slice, 2], color='blue')

    ax.view_init(elev=90., azim=90.)
    ax.set_xlim(ax.get_xlim()[::-1])
    ax.title.set_text("input landmarks")

    # transformed plot





    mat = umeyama(centered_face,landmarks_68_3D, True)
    transformed_face = transform_points(centered_face,mat,False)





    ax = fig.add_subplot(2, 3, 3, projection='3d')
    surf = ax.scatter(transformed_face[:, 0],
                      transformed_face[:, 1],
                      transformed_face[:, 2],
                      c='cyan',
                      alpha=1.0,
                      edgecolor='b')

    for pred_type in pred_types.values():
        ax.plot3D(transformed_face[pred_type.slice, 0],
                  transformed_face[pred_type.slice, 1],
                  transformed_face[pred_type.slice, 2], color='blue')

    ax.view_init(elev=90., azim=90.)
    ax.set_xlim(ax.get_xlim()[::-1])
    ax.title.set_text("transformed input")

    ax = fig.add_subplot(2, 3, 4, projection='3d')



    inverted_68 = transform_points(landmarks_68_3D, mat, True)








    surf = ax.scatter(inverted_68[:, 0],
                      inverted_68[:, 1],
                      inverted_68[:, 2],
                      c='cyan',
                      alpha=1.0,
                      edgecolor='b')

    for pred_type in pred_types.values():
        ax.plot3D(inverted_68[pred_type.slice, 0],
                  inverted_68[pred_type.slice, 1],
                  inverted_68[pred_type.slice, 2], color='blue')
    ax.title.set_text("inverted n68")

    ax.view_init(elev=90., azim=90.)
    ax.set_xlim(ax.get_xlim()[::-1])

    ####the input image
    ax = fig.add_subplot(2, 3, 5)
    ax.imshow(input_img)

    for pred_type in pred_types.values():
        ax.plot(inverted_68[pred_type.slice, 0],
                inverted_68[pred_type.slice, 1],
                color=pred_type.color)
    ax.axis('off')
    ax.title.set_text("inverted face")

    ####the input image
    ax = fig.add_subplot(2, 3, 6)
    ax.imshow(input_img)
    for pred_type in pred_types.values():
        ax.plot(facepoints[pred_type.slice, 0],
                facepoints[pred_type.slice, 1],
                color=pred_type.color)
    ax.axis('off')
    ax.title.set_text("normal face")

    plt.show()
