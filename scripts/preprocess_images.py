import json

import face_alignment
import pandas as pd

from core.leras import nn
from hparams import HParams
from src.spath import *
import matplotlib.pyplot as plt
from tqdm import tqdm
import subprocess
from src.smath import get_umeyama_transformed


hparams = HParams(
    batch_size=32,
    place_model_on_cpu=False,
    fps=25,
)
DEBUG = False

class WAVImage:
    def __init__(self, image, idx, file_path, settings, sound_frame, rotation=None, rect=None, landmarks=None,
                 rotated_image=None):
        self.image = image
        self.rotation = rotation
        self.rotated_image = rotated_image
        self.landmarks = landmarks
        self.aligned_landmarks = None

        self.transformed_image = None

        self.debug_image = None
        self.rect = rect
        self.idx = idx
        self.file_path = file_path
        self.settings = settings
        self.cleaned_path = os.path.splitext(os.path.relpath(file_path, settings['vid_path']))[0]
        self.soundframe = sound_frame
        self.name = str(idx).zfill(5)

    def store_debug_image(self):
        p = self.settings['output_debug_path'].joinpath(self.cleaned_path)
        p.mkdir(parents=True, exist_ok=True)
        cv2_imwrite(p / f"{self.name}.jpg", self.debug_image, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
        if(DEBUG):io.log_info("written debug:" + str(p) + "\\" + str(self.name))

    def store_data(self):
        p = self.settings['output_path'].joinpath(self.cleaned_path)
        p.mkdir(parents=True, exist_ok=True)
        cv2_imwrite(p / f"{self.name}.jpg", self.image, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
        if(DEBUG):io.log_info("written:" + str(p) + "\\" + str(self.name))

        d = {
            "sound_frame": self.soundframe,
            "landmarks": self.landmarks.tolist(),
            "landmarks_aligned": self.aligned_landmarks.tolist(),
        }
        with open(p / f"{self.name}.json", 'w') as f:
            json.dump(d, f)


FAN = None


def init_models(device_config):
    global FAN
    FAN = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D, flip_input=False)


class NoFaceException(Exception):
    pass

def extract_landmarks(wImage):
    #can add batch
    preds = FAN.get_landmarks(wImage.image)
    if not preds:
        io.log_err("no face detected:" + str(wImage.file_path))
        raise NoFaceException
    wImage.landmarks = preds[0]
    wImage.aligned_landmarks = get_umeyama_transformed(wImage.landmarks,False)



def analyze_text_file(file_path):
    with open(file_path) as f:
        sentence = f.readline()
        confidence = f.readline()
        f.readline()  # get rid of the space
        f.readline()  # get rid of description

        boundaries = []
        while (l := f.readline()):
            boundaries.append(l.split())

        df = pd.DataFrame(data=boundaries, columns=["words", "start", "end", "asdscore"])
        df = df.astype({'start': 'double', 'end': 'double', 'asdscore': 'double'})

        return df

def get_relevant_frame(df: pd.DataFrame, idx):
    stepsize = 1 / hparams.fps

    # a bit of leniency, the frame ranges over 1 step size
    output = df['start'] <= stepsize * (idx+1)
    output2 = df['end'] >= stepsize * idx
    l = df[output & output2]
    return l.values[0].tolist() if len(l) > 0 else []



def plot3d(wImage):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax = fig.add_subplot(1, 2, 2, projection='3d')
    surf = ax.scatter(wImage.landmarks[:, 0] * 1.2,
                      wImage.landmarks[:, 1],
                      wImage.landmarks[:, 2],
                      c='cyan',
                      alpha=1.0,
                      edgecolor='b')

    plt.show()

    # o3d.visualization.draw_geometries(wImage.landmarks),
    pass

def process_video(file_path: Path, settings):
    cleaned_path = os.path.splitext(os.path.relpath(str(file_path), settings['vid_path']))[0]
    p = settings['output_path'].joinpath(cleaned_path)
    write_spec(p,settings)
    extract_audio(file_path , p)
    df = analyze_text_file(os.path.splitext(str(file_path))[0] + ".txt")  # open the txt file
    video_stream = cv2.VideoCapture(str(file_path))

    idx = 0
    while 1:
        still_reading, frame = video_stream.read()
        if not still_reading:
            video_stream.release()
            break

        rf = get_relevant_frame(df, idx)
        wImage = WAVImage(frame, idx, file_path, settings, rf)
        # extract_faces(wImage)
        try:
            extract_landmarks(wImage)
        except NoFaceException:
            continue

        wImage.store_data()

        idx += 1

def write_spec(output_path,settings):
    output_path.mkdir(parents=True, exist_ok=True)
    with open(output_path/"spec.json",'w') as f:
        json.dump({**settings,
                   "fps":hparams.fps}, f,default=str)


def extract_audio(input_path,output_path):
    output_path.mkdir(parents=True, exist_ok=True)

    absout = os.path.abspath(output_path/ "audio.wav")
    absin = os.path.abspath(input_path)

    if(DEBUG):io.log_info(f"absin:{absin} absout:{absout}" )
    command = f'ffmpeg -loglevel panic -y -i {absin} -strict -2 {absout}'
    subprocess.call(command, shell=True)
    if(DEBUG):io.log_info(f"{input_path} dumped")

def main(
        vid_path=Path("data/unprocessed"),
        output_path=Path("data/processed"),
        temp_path=Path("data/temp"),
        force_gpu_idxs=None,
        cpu_only=False,
        preload=False,
):
    settings = {}
    global DEBUG
    DEBUG = settings['debug'] = io.input_bool("debug?", False) if not preload else True
    settings['vid_path'] = get_vid_path(vid_path) if not preload else vid_path
    # settings['temp_path'] = get_temp_path(temp_path) if not preload else temp_path
    settings['output_path'] = get_output_path(output_path) if not preload else output_path
    settings['output_debug_path'] = settings['output_path'].joinpath("debug") if settings['debug'] else None

    settings['image_size'] = io.input_int(f"Image size", 512, valid_range=[256, 2048],
                                          help_message="Output image size. The higher image size, the worse face-enhancer "
                                                       "works. Use higher than 512 value only if the source image is sharp enough "
                                                       "and the face does not need to be enhanced.") if not preload else 512

    nn.initialize_main_env()
    io.log_info("env initialized")

    device_config = nn.DeviceConfig.GPUIndexes(
        force_gpu_idxs or nn.ask_choose_device_idxs(suggest_all_gpu=True) if not preload else [0]) \
        if not cpu_only else nn.DeviceConfig.CPU()

    init_models(device_config)
    io.log_info("models loaded")

    filelist = glob(os.path.join(settings['vid_path'], '**/*.mp4'), recursive=True)

    for file_path in tqdm(filelist):
        process_video(Path(file_path), settings)
        if(DEBUG):io.log_info(f"{file_path} completed")


if __name__ == '__main__':
    main()