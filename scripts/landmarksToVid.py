from pathlib import Path
from glob import glob
import os
import subprocess
import matplotlib.pyplot as plt
import collections
import json
import numpy as np
from tqdm import tqdm
from skimage import io
import torch

from utils.basel import Basel

input_path = Path(r"D:\GDrive\Scriptie\wav2betterlipclean\workspace\lotr")
jsonfilelist = glob( os.path.join(input_path, '**/[0-9]*.landmarks'), recursive=True)
imagefilelist = glob( os.path.join(input_path, '**/[0-9]*.jpg'), recursive=True)

shapes = glob( os.path.join(input_path, '**/full.shape'), recursive=True)
expressions = glob( os.path.join(input_path, '**/full.expression'), recursive=True)



name = "landmarks_aligned"
transformed_landmarks = []
imgs = []

jsonfilelist.sort(key=lambda x: int(Path(x).stem))
for jsonfile in jsonfilelist:
        transformed_landmarks.append(torch.load(jsonfile).cpu().numpy())

imagefilelist.sort(key=lambda x: int(Path(x).stem))
for imgfile in imagefilelist :
        input_img = io.imread(imgfile)
        imgs.append(input_img)

basel = Basel(checkpoints="../checkpoints")
exp_shape_landmarks = []
for shape,expression in zip(shapes,expressions):
    shape_full = torch.load(shape).cpu()
    expression_full = torch.load(expression).cpu()
    for  expression in expression_full:
        exp_shape_landmarks.append(basel.get_face(shape_full,expression,normalized=True))




pred_type = collections.namedtuple('prediction_type', ['slice', 'color'])
pred_types = {'face': pred_type(slice(0, 17), (0.682, 0.780, 0.909, 0.5)),
                  'eyebrow1': pred_type(slice(17, 22), (1.0, 0.498, 0.055, 0.4)),
                  'eyebrow2': pred_type(slice(22, 27), (1.0, 0.498, 0.055, 0.4)),
                  'nose': pred_type(slice(27, 31), (0.345, 0.239, 0.443, 0.4)),
                  'nostril': pred_type(slice(31, 36), (0.345, 0.239, 0.443, 0.4)),
                  'eye1': pred_type(slice(36, 42), (0.596, 0.875, 0.541, 0.3)),
                  'eye2': pred_type(slice(42, 48), (0.596, 0.875, 0.541, 0.3)),
                  'lips': pred_type(slice(48, 60), (0.596, 0.875, 0.541, 0.3)),
                  'teeth': pred_type(slice(60, 68), (0.596, 0.875, 0.541, 0.4))
                  }

def generate_landmark_video(lmrks,p,invert=True):
    tmp = p/"temp"
    tmp.mkdir(parents=True, exist_ok=True)
    fig = plt.figure(figsize=(6, 6), dpi=100, )

    for i,lmrk in tqdm(enumerate(lmrks)):
        ax = fig.subplots()
        lmrk = np.asarray(lmrk)
        surf = ax.scatter(lmrk[:, 0],
                          lmrk[:, 1],
                          c='cyan',
                          alpha=1.0,
                          edgecolor='b')
        for pred_type in pred_types.values():
            ax.plot(lmrk[pred_type.slice, 0],
                    lmrk[pred_type.slice, 1],
                        color=pred_type.color)

        if invert:ax.invert_yaxis()
        plt.savefig(tmp / ("%04d.png" % i))
        fig.clf()

    subprocess.call([
        'ffmpeg', '-framerate', '25', '-i', tmp/'%04d.png', '-r', '10', '-pix_fmt', 'yuv420p',
        p/f'{name}.mp4'
    ])

def generate_overlay_video(lmrks,imgs, p):
        tmp = p / "temp_overlay"
        tmp.mkdir(parents=True, exist_ok=True)
        fig = plt.figure(figsize=(6, 6), dpi=100, )

        for i, (lmrk,img) in tqdm(enumerate(zip(lmrks,imgs))):
            ax = fig.subplots()
            lmrk = np.asarray(lmrk)
            ax.imshow(img)
            surf = ax.scatter(lmrk[:, 0],
                              lmrk[:, 1],
                              c='cyan',
                              alpha=1.0,
                              edgecolor='b')
            for pred_type in pred_types.values():
                ax.plot(lmrk[pred_type.slice, 0],
                        lmrk[pred_type.slice, 1],
                        color=pred_type.color)

            plt.savefig(tmp / ("file%04d.png" % i))
            fig.clf()

        subprocess.call([
            'ffmpeg', '-framerate', '25', '-i', tmp / 'file%04d.png', '-r', '10', '-pix_fmt', 'yuv420p',
                                                p / f'{name}_overlay.mp4'
        ])


# generate_landmark_video(transformed_landmarks,input_path)
# generate_landmark_video(exp_shape_landmarks,input_path,False)
generate_overlay_video(transformed_landmarks,imgs,input_path)
