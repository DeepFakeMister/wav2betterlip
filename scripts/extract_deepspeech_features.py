from pathlib import Path

from src.audio_feature_extractor import get_audio_handler,run_audio_handler
from src.utils.spath import *
from tqdm import tqdm

# import argparse
#
# parser = argparse.ArgumentParser(description='Extract audio features')
# parser.add_argument('--ds_fname', default='../../checkpoints/vicab.pb', help='Path to trained DeepSpeech model')
# # parser.add_argument('--audio_fname', default='./audio/test_sentence.wav', help='Path of input speech sequence')
# parser.add_argument('--audiofiles', default=filename, help='Path of input speech sequence')
# parser.add_argument('--out_path', default='./output', help='Output path')
# parser.add_argument('--target_fps', default=25, help='Target frame rate')


AUDIO_HANDLER = None
def init_models():
    global AUDIO_HANDLER
    AUDIO_HANDLER = get_audio_handler("../../checkpoints/voca.pb")

def extract_audio(file_path,settings):
    out_path = output_path_maintain_structure(settings['wav_path'],file_path,settings['out_path'])
    processed_audio  = run_audio_handler(AUDIO_HANDLER,file_path,25)


    #fps does not match the video framerate
    print('audio processed type:', type(processed_audio))
    print('audio processed shape:', processed_audio.shape)
    num_frames = processed_audio.shape[0]
    print('num_frames:', num_frames)

    # Visualize
    # import matplotlib.pyplot as plt
    # from matplotlib import cm
    # fig, ax = plt.subplots()
    # mfcc_data = np.swapaxes(processed_audio[0], 0, 1)
    # cax = ax.imshow(processed_audio[0], interpolation='nearest', cmap=cm.coolwarm, origin='lower')
    # plt.show()

    for i in range(0, num_frames):
        fname = out_path / (str(i).zfill(5) + 'deepspeech')
        np.save(fname, processed_audio[i])
        # np.savetxt(fname, processed_audio[i], delimiter=',')


def main(
        wav_path=Path("../../data/processed"),
        output_path=Path("../../data/processed"),
        temp_path=Path("data/temp"),
        force_gpu_idxs=None,
        cpu_only=False,
        preload=True,
):
    settings = {}
    global DEBUG
    DEBUG = settings['debug'] = io.input_bool("debug?", False) if not preload else True
    settings['wav_path'] = get_vid_path(wav_path) if not preload else wav_path
    settings['out_path'] = get_output_path(output_path) if not preload else output_path

    filelist = glob(os.path.join(settings['wav_path'], '**/*.wav'), recursive=True)


    init_models()
    io.log_info("models loaded")

    for file_path in tqdm(filelist):
        extract_audio(abspath(file_path),settings)


if __name__ == "__main__":
    main()