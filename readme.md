# Wav2BetterLip
In this repo we outline Wav2betterLip.
To preprocess the data in the right format, check the preprocessing folder.

There are three relevant notebooks /notebook.

- LipRetrieval-mel.ipynb -  trains  Audio2FacialLandmarks model based on mel-spectogram features 
- Wav2BetterLip.ipynb - Trains the full Wav2BetterLip model.
- Wav2BetterLipOutput.ipynb - generates the output for the Wav2BetterLip model.

# Installation
The notebooks should be self contained, the correct repo can be setup using:
```sh
pip install -r requirements.txt
```

---

| Name | Link |
| ------ | ------ |
| wav2lip | [https://github.com/Rudrabha/Wav2Lip](https://github.com/Rudrabha/Wav2Lip) |
| Paper |  https://drive.google.com/drive/folders/14eN5X8Knm4Eywi5BIvEJ4mp8zEofnQ1g?usp=sharing|
| Sample Video's | [https://drive.google.com/drive/folders/1qgdGr7gQ9lTAdApbC2p0tRnP886kuFeW?usp=sharing](https://drive.google.com/drive/folders/1qgdGr7gQ9lTAdApbC2p0tRnP886kuFeW?usp=sharing)  |
| dataset | https://www.robots.ox.ac.uk/~vgg/data/lip_reading/lrs2.html |
| Basel Model | https://faces.dmi.unibas.ch/bfm/bfm2017.html| 

Note, this repo is still very experimental, if you want an out the box working solution, you are better of using [wav2lip](https://github.com/Rudrabha/Wav2Lip).

